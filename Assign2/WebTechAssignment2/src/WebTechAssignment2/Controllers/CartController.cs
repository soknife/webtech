using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebTechAssignment2.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;

namespace WebTechAssignment2.Controllers
{
    public class CartController : Controller {

        private readonly CineplexContext _context;

        public CartController(CineplexContext context) {
            _context = context;
        }

        // GET: Cart
        public ActionResult Index() {
            ViewData["Header"] = "Cart";




            return View();
        }

        // Returns a view with option to add a booking
        public async Task<ActionResult> CreateBooking(int? movieId, int? cineplexId, int? sessionId) {
            // Get current session's cart
            Cart cart = Cart.GetSessionCart(HttpContext);
            if (cart.Bookings.Count >= 5)
                return RedirectToAction("CannotBook");

            // Get the target movie
            IQueryable<CineplexMovie> cineplexContext = _context.CineplexMovie.Include(c => c.Cineplex).Include(c => c.Movie).Include(c => c.Session).AsQueryable();
            cineplexContext = cineplexContext.Where(c => 
                c.CineplexId == cineplexId && 
                c.MovieId == movieId && 
                c.SessionId == sessionId);
            CineplexMovie targetMovie = await cineplexContext.FirstAsync();

            // Get booking of movie
            Booking existingBooking = cart.GetBooking(targetMovie);
            Tuple<Booking, CineplexMovie> bookingAndMovie = new Tuple<Booking, CineplexMovie>(existingBooking, targetMovie);

            return View(bookingAndMovie);
        }

        public ActionResult CannotBook() {
            return View();
        }

        // Creates the booking
        public async Task<ActionResult> CompleteBooking(int? movieId, int? cineplexId, int? sessionId, string seats, int? fullPriceTickets, int? concessionPriceTickets) {
           // Get the target movie
            IQueryable<CineplexMovie> cineplexContext = _context.CineplexMovie.Include(c => c.Cineplex).Include(c => c.Movie).Include(c => c.Session).AsQueryable();
            CineplexMovie targetMovie = await cineplexContext.Where(c =>
                c.CineplexId == cineplexId &&
                c.MovieId == movieId &&
                c.SessionId == sessionId).FirstAsync();

            // Get booking of movie, and set booked seats
            Cart cart = Cart.GetSessionCart(HttpContext);
            
            if (seats == "delete") {
                // Remove booking from cart
                cart.RemoveBooking(cart.GetBooking(targetMovie));
                Cart.SetSessionCart(HttpContext, cart);

                // Redirect to view booking
                return RedirectToAction("ViewBooking");
            } else {
                // Create/update booking
                Booking finalBooking = cart.GetBooking(targetMovie);
                finalBooking.Seats = seats;
                finalBooking.FullPriceTickets = fullPriceTickets.Value;
                finalBooking.ConcessionTickets = concessionPriceTickets.Value;
                cart.SetBooking(finalBooking);

                // Set cart with the new booking
                Cart.SetSessionCart(HttpContext, cart);

                // Show booking details
                return View(finalBooking);
            }
        }

        // Views and allows editing of existing bookings
        public async Task<ActionResult> ViewBooking() {

            Cart cart = Cart.GetSessionCart(HttpContext);
            IQueryable<CineplexMovie> cineplexContext = _context.CineplexMovie.Include(c => c.Cineplex).Include(c => c.Movie).Include(c => c.Session).AsQueryable();

            // Get all bookings and their movie data
            foreach (Booking booking in cart.Bookings) {
                // Get movie of booking
                CineplexMovie movie = await cineplexContext.Where(c =>
                    c.CineplexId == booking.CineplexId &&
                    c.MovieId == booking.MovieId &&
                    c.SessionId == booking.SessionId).FirstAsync();

                booking.Cineplex = movie.Cineplex;
                booking.Movie = movie.Movie;
                booking.Session = movie.Session;
            }

            return View(cart.Bookings);
        }

        // Reviews purchase and requests credit card information
        public ActionResult Checkout() {
            if (Cart.GetSessionCart(HttpContext).Bookings.Count == 0)
                return RedirectToAction("ViewBooking");

            if (HttpContext.User != null &&
                HttpContext.User.Identity.IsAuthenticated) {
                return View(Cart.GetSessionCart(HttpContext).Bookings);
            }

            return RedirectToAction("Login", "Account", new { returnUrl = "~/Cart/Checkout"});
        }

        // Writes booking to db, and empties cart
        public async Task<ActionResult> CompleteTransaction() {
            Cart cart = Cart.GetSessionCart(HttpContext);
            IQueryable<CineplexMovie> cineplexContext = _context.CineplexMovie.Include(c => c.Cineplex).Include(c => c.Movie).Include(c => c.Session).AsQueryable();

            // Retain booking for transaction summary
            List<Booking> bookings = cart.Bookings.ToList();

            Booking lastBooking = await _context.Booking.LastOrDefaultAsync();
            int lastId = (lastBooking == null) ? 0 : lastBooking.BookingId;
            foreach (Booking booking in cart.Bookings) {
                booking.BookingId = ++lastId;

                CineplexMovie movie = await cineplexContext.Where(c =>
                    c.CineplexId == booking.CineplexId &&
                    c.MovieId == booking.MovieId &&
                    c.SessionId == booking.SessionId).FirstAsync();

                // Assign booked seats to movies
                string[] occupiedSeats = movie.Seats.Split(',');
                string[] bookedSeats = booking.Seats.Split(',');
                for (int s = 0; s < occupiedSeats.Length &&  s < bookedSeats.Length; s++) {
                    if (bookedSeats[s] == "1")
                        occupiedSeats[s] = "1";
                }
                movie.Seats = String.Join(",", occupiedSeats);

                _context.Update(movie);
                
                _context.Add(booking);

                try {
                    await _context.SaveChangesAsync();
                } catch (DbUpdateConcurrencyException e) {
                    Console.WriteLine(e.StackTrace);
                    return View(null);
                }
            }

            
            
            // Add bookings
            //_context.AddRange(cart.Bookings);
            

            // Clear cart of bookings
            cart.Bookings.Clear();
            Cart.SetSessionCart(HttpContext, cart);

            return View(bookings);
        }
                    
    }
}