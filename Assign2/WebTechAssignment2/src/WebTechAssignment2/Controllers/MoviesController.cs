using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebTechAssignment2.Models;
using Microsoft.Extensions.Configuration;
using WebTechAssignment2.Models.MoviesViewModels;

namespace WebTechAssignment2.Controllers
{
    public class MoviesController : Controller
    {
        private readonly CineplexContext _context;

        public MoviesController(CineplexContext context)
        {
            _context = context;
        }

        // Returns a view with data sorted by movies.
        // Filters data if search string is not empty
        public async Task<IActionResult> Index(string searchString) {
            IQueryable<CineplexMovie> cineplexMovieContext = _context.CineplexMovie.Include(c => c.Cineplex).Include(c => c.Movie).AsQueryable();
            IQueryable<MovieComingSoon> comingSoonContext = _context.MovieComingSoon.AsQueryable();

            // Filter data by search string if applicable
            if (!String.IsNullOrEmpty(searchString)) {
                cineplexMovieContext = cineplexMovieContext.Where(x => 
                    x.Movie.Title.Contains(searchString) ||
                    x.Movie.ShortDescription.Contains(searchString) ||
                    x.Cineplex.Name.Contains(searchString) ||
                    x.Cineplex.Location.Contains(searchString));
                comingSoonContext = comingSoonContext.Where(m =>
                    m.Title.Contains(searchString) ||
                    m.ShortDescription.Contains(searchString) ||
                    m.LongDescription.Contains(searchString));
                ViewData["searchString"] = searchString;
            } else {
                ViewData["searchString"] = "";
            }
            List<CineplexMovie> filteredCineplexMovie = await cineplexMovieContext.ToListAsync();
            List<MovieComingSoon> movieCommingSoon = await comingSoonContext.ToListAsync();

            // Sort cineplex data by movie
            Dictionary<Movie, List<Models.Cineplex>> cineplexByMovie = new Dictionary<Movie, List<Models.Cineplex>>();
            foreach (CineplexMovie cineplexMovie in filteredCineplexMovie) {
                // Add cineplex to list of showing cinemas
                if (cineplexByMovie.ContainsKey(cineplexMovie.Movie)) {
                    // Only add if cinema hasn't been added already
                    if (!cineplexByMovie[cineplexMovie.Movie].Exists(c => c.CineplexId == cineplexMovie.CineplexId))
                        cineplexByMovie[cineplexMovie.Movie].Add(cineplexMovie.Cineplex);
                } else {
                    // Create a new list of showing cinemas if this is the first cinema to be added
                    cineplexByMovie.Add(cineplexMovie.Movie, new List<Models.Cineplex>() { cineplexMovie.Cineplex });
                }
            }
            
            AllMovies allMovies = new AllMovies(cineplexByMovie, movieCommingSoon);

            return View(allMovies);
        }

        // Returns a view with data filtered by a specific movie
        public async Task<IActionResult> MovieDetail(int? movieID) {
            IQueryable<CineplexMovie> cineplexContext = _context.CineplexMovie.Include(c => c.Cineplex).Include(c => c.Movie).Include(c => c.Session).AsQueryable();
            cineplexContext = cineplexContext.Where(c => c.MovieId == movieID);

            List<CineplexMovie> specificMovie = await cineplexContext.ToListAsync();

            // Sort session data by cineplex
            Dictionary<Models.Cineplex, List<Session>> sessionByCineplex = new Dictionary<Models.Cineplex, List<Session>>();
            foreach (CineplexMovie cineplexMovie in specificMovie) {
                if (sessionByCineplex.ContainsKey(cineplexMovie.Cineplex))
                    sessionByCineplex[cineplexMovie.Cineplex].Add(cineplexMovie.Session);
                else
                    sessionByCineplex.Add(cineplexMovie.Cineplex, new List<Session>() { cineplexMovie.Session });
            }

            return View(new MovieCineplexSession(specificMovie[0].Movie, sessionByCineplex));
        }

        public async Task<IActionResult> UpcomingMovieDetail(int? movieID) {
            return View(await _context.MovieComingSoon.Where(m => m.MovieComingSoonId == movieID).FirstAsync());
        }
    }
}
