﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebTechAssignment2.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using WebTechAssignment2.Models.APIModels;

namespace WebTechAssignment2.Controllers.API
{
    [Route("api/[controller]")]
    public class MovieController : Controller
    {
        private readonly CineplexContext _context;
        private IConfiguration _configuration;

        public MovieController(CineplexContext context, IConfiguration configuration) {
            _context = context;
            _configuration = configuration;
        }
        
        [HttpGet]
        public async Task<IEnumerable<MoviesAPIModel>> Get() {
            List<MoviesAPIModel> movies = new List<MoviesAPIModel>();
            foreach(Movie movie in await _context.Movie.ToListAsync()) {
                movies.Add(await Get(movie.MovieId));
            }

            return movies;
        }
        
        [HttpGet("{id}")]
        public async Task<MoviesAPIModel> Get(int id) {
            // Returns movie plus the cineplex ids where's it's currently airing
            MoviesAPIModel movie = new MoviesAPIModel(
                await _context.Movie.Where(m => m.MovieId == id).FirstAsync());
            foreach(CineplexMovie cineplexMovie in 
                await _context.CineplexMovie.Where(m => m.MovieId == id).Include(m => m.Session).ToListAsync()) {
                movie.CineplexId.Add(new SessionAPIModel(
                    cineplexMovie.CineplexId,
                    cineplexMovie.Session.StartingTime.ToString(@"hh\:mm"),
                    cineplexMovie.Session.EndingTime.ToString(@"hh\:mm")));
            }

            return movie;
        }
        
        [HttpPost]
        public async Task<string> Post([FromBody]Movie movie) {
            if (ModelState.IsValid) {
                try {
                    _context.Add(movie);
                    await _context.SaveChangesAsync();
                } catch (DbUpdateConcurrencyException e) {
                    Console.WriteLine(e.StackTrace);
                    return "Unable to add \"" + movie.Title + "\" #";
                }

                return "\"" + movie.Title + "\" added successfully!";
            }

            return "Unable to add \"" + movie.Title + "\" #";
        }
        
        [HttpPut("{id}")]
        public async Task<string> Put(int id, [FromBody]Movie movie) {
            if (id != movie.MovieId) {

                return "Unable to edit \"" + movie.Title + "\" #";
            }

            if (ModelState.IsValid) {
                try {
                    if (!_context.Movie.Any(m => m.MovieId == id)) {
                        return "Unable to edit \"" + movie.Title + "\" #";
                    }

                    _context.Update(movie);
                    await _context.SaveChangesAsync();

                    return "\"" + movie.Title + "\" editted successfully!";

                } catch (DbUpdateConcurrencyException e) {
                    Console.WriteLine(e.StackTrace);
                    return "Unable to edit \"" + movie.Title + "\" #";
                }
            }

            return "Unable to edit \"" + movie.Title + "\" #";
        }
        
        [HttpDelete("{id}")]
        public async Task<string> Delete(int id) {
            Movie movie = await _context.Movie.Where(m => m.MovieId == id).FirstAsync();

            try {
                _context.Movie.Remove(movie);
                await _context.SaveChangesAsync();
            } catch (DbUpdateConcurrencyException e) {
                Console.WriteLine(e.StackTrace);
                return "Unable to delete \"" + movie.Title + "\" #";
            }
            
            return "\"" + movie.Title + "\" deleted successfully!";
        }
    }
}
