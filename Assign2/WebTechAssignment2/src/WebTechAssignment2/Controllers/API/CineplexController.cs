﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebTechAssignment2.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using WebTechAssignment2.Models.APIModels;

namespace WebTechAssignment2.Controllers {
    [Route("api/[controller]")]
    public class CineplexController : Controller {
        private readonly CineplexContext _context;
        private IConfiguration _configuration;

        public CineplexController(CineplexContext context, IConfiguration configuration) {
            _context = context;
            _configuration = configuration;
        }

        [HttpGet]
        public async Task<IEnumerable<CineplexAPIModel>> Get() {
            List<CineplexAPIModel> cineplexs = new List<CineplexAPIModel>();
            foreach (Cineplex cineplex in await _context.Cineplex.ToListAsync()) {
                cineplexs.Add(await Get(cineplex.CineplexId));
            }

            return cineplexs;
        }

        [HttpGet("{id}")]
        public async Task<CineplexAPIModel> Get(int id) {
            // Returns movie plus the cineplex ids where's it's currently airing
            CineplexAPIModel cineplex = new CineplexAPIModel(
                await _context.Cineplex.Where(m => m.CineplexId == id).FirstAsync());
            foreach (CineplexMovie cineplexMovie in
                await _context.CineplexMovie.Where(m => m.CineplexId == id).Include(m => m.Session).ToListAsync()) {
                cineplex.MovieId.Add(new SessionAPIModel(
                    cineplexMovie.MovieId,
                    cineplexMovie.Session.StartingTime.ToString(@"hh\:mm"),
                    cineplexMovie.Session.EndingTime.ToString(@"hh\:mm")
                    ));
            }

            return cineplex;
        }

        [HttpPost]
        public async Task<string> Post([FromBody] Cineplex cineplex) {
            if (ModelState.IsValid) {
                try {
                    _context.Add(cineplex);
                    await _context.SaveChangesAsync();
                } catch (DbUpdateConcurrencyException e) {
                    Console.WriteLine(e.StackTrace);
                    return "Unable to add \"" + cineplex.Name + "\" #";
                }

                return "\"" + cineplex.Name + "\" added successfully!";
            }

            return "Unable to add \"" + cineplex.Name + "\" #";
        }

        [HttpPut("{id}")]
        public async Task<string> Put(int id, [FromBody] Cineplex cineplex) {
            if (id != cineplex.CineplexId) {

                return "Unable to edit \"" + cineplex.Name + "\" #";
            }

            if (ModelState.IsValid) {
                try {
                    if (!_context.Cineplex.Any(m => m.CineplexId == id)) {
                        return "Unable to edit \"" + cineplex.Name + "\" #";
                    }

                    _context.Update(cineplex);
                    await _context.SaveChangesAsync();

                    return "\"" + cineplex.Name + "\" editted successfully!";

                } catch (DbUpdateConcurrencyException e) {
                    Console.WriteLine(e.StackTrace);
                    return "Unable to edit \"" + cineplex.Name + "\" #";
                }
            }

            return "Unable to edit \"" + cineplex.Name + "\" #";
        }

        [HttpDelete("{id}")]
        public async Task<string> Delete(int id) {
            Cineplex cineplex = await _context.Cineplex.Where(m => m.CineplexId == id).FirstAsync();

            try {
                _context.Cineplex.Remove(cineplex);
                await _context.SaveChangesAsync();
            } catch (DbUpdateConcurrencyException e) {
                Console.WriteLine(e.StackTrace);
                return "Unable to delete \"" + cineplex.Name + "\" #";
            }

            return "\"" + cineplex.Name + "\" deleted successfully!";
        }
    }
}
