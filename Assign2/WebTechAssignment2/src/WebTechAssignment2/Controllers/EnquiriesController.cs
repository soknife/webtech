using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebTechAssignment2.Models;

namespace WebTechAssignment2.Controllers
{
    public class EnquiriesController : Controller
    {
        private readonly CineplexContext _context;

        public EnquiriesController(CineplexContext context)
        {
            _context = context;    
        }

        // GET: Enquiries/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Enquiries/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("EnquiryId,Email,Message")] Enquiry enquiry)
        {
            if (ModelState.IsValid)
            {
                _context.Add(enquiry);
                await _context.SaveChangesAsync();
                return RedirectToAction("Confirm", new { enquiry.Email });
            }
            return View(enquiry);
        }

        public IActionResult Confirm(string email) {
            ViewData["Email"] = email;
            return View();
        }
    }
}
