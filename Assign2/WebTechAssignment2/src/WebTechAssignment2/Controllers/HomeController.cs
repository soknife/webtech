﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using WebTechAssignment2.Models;
using Microsoft.EntityFrameworkCore;

using Microsoft.Extensions.Configuration;

namespace WebTechAssignment2.Controllers
{
    public class HomeController : Controller
    {

        private readonly CineplexContext _context;
        private IConfiguration _configuration;

        public HomeController(CineplexContext context, IConfiguration configuration) {
            _context = context;
            _configuration = configuration;
        }

        public async Task<IActionResult> Index()
        {
            // Simple cinplex location data for about text
            List<string> cineplexs = new List<string>();
            foreach (Cineplex cineplex in await _context.Cineplex.ToListAsync())
                cineplexs.Add(cineplex.Location);
            ViewData["Cineplex"] = cineplexs;

            // Airing movies, up to 3
            List<Movie> movies = await _context.Movie.Take(Math.Min(_context.Movie.Count(), 3)).ToListAsync();

            return View(movies);
        }

        public IActionResult Readme()
        {
            return View();
        }
    }
}
