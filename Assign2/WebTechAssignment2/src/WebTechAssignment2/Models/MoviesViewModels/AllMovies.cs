﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebTechAssignment2.Models.MoviesViewModels
{
    public class AllMovies {
        public Dictionary<Movie, List<Cineplex>> Released { get; private set; }
        public IEnumerable<MovieComingSoon> Upcoming { get; private set; }

        public AllMovies(Dictionary<Movie, List<Cineplex>> released, IEnumerable<MovieComingSoon> upcoming) {
            Released = released;
            Upcoming = upcoming;
        }
    }
}
