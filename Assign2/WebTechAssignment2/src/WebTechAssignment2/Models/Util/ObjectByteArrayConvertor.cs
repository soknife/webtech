﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;

namespace WebTechAssignment2.Models.Util
{
    public class ObjectByteArrayConvertor
    {
        public static byte[] Serialize<T>(T obj) {
            if (obj == null)
                throw new NullReferenceException("Object to be serialized cannot be null");
            BinaryFormatter formatter = new BinaryFormatter();
            using (MemoryStream stream = new MemoryStream()) {
                formatter.Serialize(stream, obj);
                return stream.ToArray();
            }
        }

        public static T Deserialize<T>(byte[] data) {
            if (data == null)
                return default(T);
            BinaryFormatter formatter = new BinaryFormatter();
            using (MemoryStream stream = new MemoryStream(data)) {
                object result = formatter.Deserialize(stream);
                return (T)result;
            }
        }
    }
}
