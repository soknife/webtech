﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebTechAssignment2.Models
{
    // Details of a movie, and it's cineplex location + session details
    public class MovieCineplexSession {
        public Movie Movie { get; set; }
        public Dictionary<Cineplex, List<Session>> CineplexSessions { get; set; }

        public MovieCineplexSession(Movie movie, Dictionary<Cineplex, List<Session>> cineplexSessions) {
            Movie = movie;
            CineplexSessions = cineplexSessions;
        }
    }
}
