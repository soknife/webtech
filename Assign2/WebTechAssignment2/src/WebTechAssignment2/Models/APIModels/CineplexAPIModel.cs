﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebTechAssignment2.Models.APIModels
{
    public class CineplexAPIModel
    {
        public int CineplexId { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
        public string ImageUrl { get; set; }

        public List<SessionAPIModel> MovieId { get; set; }

        public CineplexAPIModel(Cineplex cineplex) {
            CineplexId = cineplex.CineplexId;
            Name = cineplex.Name;
            Location = cineplex.Location;
            ShortDescription = cineplex.ShortDescription;
            LongDescription = cineplex.LongDescription;
            ImageUrl = cineplex.ImageUrl;
            MovieId = new List<SessionAPIModel>();
        }

        public Cineplex toCineplex() {
            Cineplex cineplex = new Cineplex();
            cineplex.CineplexId = CineplexId;
            cineplex.Name = Name;
            cineplex.Location = Location;
            cineplex.ShortDescription = ShortDescription;
            cineplex.LongDescription = LongDescription;
            cineplex.ImageUrl = ImageUrl;
            return cineplex;
        }
    }
}
