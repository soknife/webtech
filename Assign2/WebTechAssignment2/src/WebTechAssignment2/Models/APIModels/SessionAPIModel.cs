﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebTechAssignment2.Models.APIModels
{
    public class SessionAPIModel
    {
        public int RelativeId { get; set; } // Id represents either movie or cineplex
        public string StartingTime { get; set; }
        public string EndingTime { get; set; }

        public SessionAPIModel(int id, string start, string end) {
            RelativeId = id;
            StartingTime = start;
            EndingTime = end;
        }
    }
}
