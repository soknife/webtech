﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebTechAssignment2.Models.APIModels
{
    public class MoviesAPIModel
    {
        public int MovieId { get; set; }
        public string Title { get; set; }
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
        public string ImageUrl { get; set; }
        public decimal FullPrice { get; set; }
        public decimal ConcessionPrice { get; set; }

        public List<SessionAPIModel> CineplexId { get; set; }

        public MoviesAPIModel(Movie movie) {
            MovieId = movie.MovieId;
            Title = movie.Title;
            ShortDescription = movie.ShortDescription;
            LongDescription = movie.LongDescription;
            ImageUrl = movie.ImageUrl;
            FullPrice = movie.FullPrice;
            ConcessionPrice = movie.ConcessionPrice;
            CineplexId = new List<SessionAPIModel>();
        }

        public Movie toMovie() {
            Movie movie = new Movie();
            movie.MovieId = MovieId;
            movie.Title = Title;
            movie.ShortDescription = ShortDescription;
            movie.LongDescription = LongDescription;
            movie.ImageUrl = ImageUrl;
            movie.FullPrice = FullPrice;
            movie.ConcessionPrice = ConcessionPrice;
            return movie;
        }
    }
}
