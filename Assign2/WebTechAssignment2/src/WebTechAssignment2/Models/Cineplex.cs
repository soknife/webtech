﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebTechAssignment2.Models
{
    [Serializable]
    public partial class Cineplex
    {
        public Cineplex()
        {
            Booking = new HashSet<Booking>();
            CineplexMovie = new HashSet<CineplexMovie>();
        }

        [Key]
        public int CineplexId { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
        public string ImageUrl { get; set; }

        public virtual ICollection<Booking> Booking { get; set; }
        public virtual ICollection<CineplexMovie> CineplexMovie { get; set; }
    }
}
