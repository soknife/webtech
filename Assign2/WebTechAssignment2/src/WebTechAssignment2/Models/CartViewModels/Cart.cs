﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using WebTechAssignment2.Models.Util;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;

namespace WebTechAssignment2.Models
{
    [Serializable]
    public class Cart {
        public List<Booking> Bookings { get; private set; }
        public string TestString { get; set; }

        private Cart() {
            Bookings = new List<Booking>();
        }

        // Finds a booking based on movie, otherwise return a new booking
        public Booking GetBooking(CineplexMovie movie) {
            Booking booking = Bookings.Find(x =>
                x.CineplexId == movie.CineplexId &&
                x.MovieId == movie.MovieId &&
                x.SessionId == movie.SessionId);
            if (booking == null) {
                booking = new Booking();
                booking.CineplexId = movie.CineplexId;
                booking.Cineplex = movie.Cineplex;
                booking.MovieId = movie.MovieId;
                booking.Movie = movie.Movie;
                booking.SessionId = movie.SessionId;
                booking.Session = movie.Session;
                booking.Seats = "0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0";
                booking.FullPriceTickets = 2;
                booking.ConcessionTickets = 0;
            }
            return booking;
        }

        // Sets the booking, replace if necessary
        public void SetBooking(Booking booking) {
            int existingIndex = Bookings.FindIndex(x =>
                x.MovieId == booking.MovieId &&
                x.SessionId == booking.SessionId &&
                x.CineplexId == booking.CineplexId);

            if (existingIndex == -1)
                Bookings.Add(booking);
            else
                Bookings[existingIndex] = booking;
        }

        // Removes booking 
        public void RemoveBooking(Booking booking) {
            int existingIndex = Bookings.FindIndex(x =>
                x.MovieId == booking.MovieId &&
                x.SessionId == booking.SessionId &&
                x.CineplexId == booking.CineplexId);

            if (existingIndex != -1)
                Bookings.RemoveAt(existingIndex);
        }

        // Attemptes to retrieve the current sessions cart, create a new cart if that doesnt exist
        public static Cart GetSessionCart(HttpContext httpContext) {
            Cart cart = ObjectByteArrayConvertor.Deserialize<Cart>(
                httpContext.Session.Get("Cart"));
            if (cart == null) {
                cart = new Cart();
            }
            return cart;
        }

        // Sets the current sessions cart
        public static void SetSessionCart(HttpContext httpContext, Cart cart) {
            httpContext.Session.Set("Cart",
                ObjectByteArrayConvertor.Serialize<Cart>(cart));
        }
    }
}
