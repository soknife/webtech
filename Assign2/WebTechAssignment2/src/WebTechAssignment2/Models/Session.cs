﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebTechAssignment2.Models
{
    [Serializable]
    public partial class Session
    {
        public Session()
        {
            Booking = new HashSet<Booking>();
            CineplexMovie = new HashSet<CineplexMovie>();
        }

        [Key]
        public int SessionId { get; set; }
        [DisplayFormat(DataFormatString = "HH:mm")]
        public TimeSpan StartingTime { get; set; }
        [DisplayFormat(DataFormatString = "HH:mm")]
        public TimeSpan EndingTime { get; set; }

        public virtual ICollection<Booking> Booking { get; set; }
        public virtual ICollection<CineplexMovie> CineplexMovie { get; set; }
    }
}
