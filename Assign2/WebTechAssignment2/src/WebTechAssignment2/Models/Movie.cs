﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebTechAssignment2.Models
{
    [Serializable]
    public partial class Movie
    {
        public Movie()
        {
            Booking = new HashSet<Booking>();
            CineplexMovie = new HashSet<CineplexMovie>();
        }

        [Key]
        public int MovieId { get; set; }
        public string Title { get; set; }
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
        public string ImageUrl { get; set; }
        public decimal FullPrice { get; set; }
        public decimal ConcessionPrice { get; set; }

        public virtual ICollection<Booking> Booking { get; set; }
        public virtual ICollection<CineplexMovie> CineplexMovie { get; set; }
    }
}
