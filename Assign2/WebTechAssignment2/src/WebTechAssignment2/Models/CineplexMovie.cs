﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebTechAssignment2.Models
{
    [Serializable]
    public partial class CineplexMovie
    {
        [Key]
        public int CineplexId { get; set; }
        [Key]
        public int MovieId { get; set; }
        [Key]
        public int SessionId { get; set; }
        public string Seats { get; set; }

        public virtual Cineplex Cineplex { get; set; }
        public virtual Movie Movie { get; set; }
        public virtual Session Session { get; set; }
    }
}
