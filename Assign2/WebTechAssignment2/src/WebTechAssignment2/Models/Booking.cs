﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebTechAssignment2.Models
{
    [Serializable]
    public partial class Booking
    {
        [Key]
        public int BookingId { get; set; }
        public int CineplexId { get; set; }
        public int MovieId { get; set; }
        public int SessionId { get; set; }
        public string Seats { get; set; }
        public int FullPriceTickets { get; set; }
        public int ConcessionTickets { get; set; }

        public virtual Cineplex Cineplex { get; set; }
        public virtual Movie Movie { get; set; }
        public virtual Session Session { get; set; }
    }
}
