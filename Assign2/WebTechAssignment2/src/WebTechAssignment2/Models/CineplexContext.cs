﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;

namespace WebTechAssignment2.Models
{
    public partial class CineplexContext : DbContext
    {
        private IConfiguration _configuration;

        public CineplexContext(IConfiguration configuration, DbContextOptions<CineplexContext> options) : base (options) {
            _configuration = configuration;
        }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_configuration.GetConnectionString("DefaultConnection"));
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Booking>(entity =>
            {
                entity.Property(e => e.BookingId)
                    .HasColumnName("BookingID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CineplexId).HasColumnName("CineplexID");

                entity.Property(e => e.ConcessionTickets).HasDefaultValueSql("'0'");

                entity.Property(e => e.FullPriceTickets).HasDefaultValueSql("'0'");

                entity.Property(e => e.MovieId).HasColumnName("MovieID");

                entity.Property(e => e.Seats)
                    .IsRequired()
                    .HasColumnType("varchar(max)")
                    .HasDefaultValueSql("'0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0'");

                entity.Property(e => e.SessionId).HasColumnName("SessionID");

                entity.HasOne(d => d.Cineplex)
                    .WithMany(p => p.Booking)
                    .HasForeignKey(d => d.CineplexId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Cineplex");

                entity.HasOne(d => d.Movie)
                    .WithMany(p => p.Booking)
                    .HasForeignKey(d => d.MovieId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Movie");

                entity.HasOne(d => d.Session)
                    .WithMany(p => p.Booking)
                    .HasForeignKey(d => d.SessionId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Session");
            });

            modelBuilder.Entity<Cineplex>(entity =>
            {
                entity.Property(e => e.CineplexId).HasColumnName("CineplexID");

                entity.Property(e => e.ImageUrl)
                    .IsRequired()
                    .HasDefaultValueSql("'~/images/default_cinema_image.png'");

                entity.Property(e => e.Location).IsRequired();

                entity.Property(e => e.LongDescription).IsRequired();

                entity.Property(e => e.Name).IsRequired();

                entity.Property(e => e.ShortDescription).IsRequired();
            });

            modelBuilder.Entity<CineplexMovie>(entity =>
            {
                entity.HasKey(e => new { e.CineplexId, e.MovieId, e.SessionId })
                    .HasName("PK_CineplexMovie_1");

                entity.Property(e => e.CineplexId).HasColumnName("CineplexID");

                entity.Property(e => e.MovieId).HasColumnName("MovieID");

                entity.Property(e => e.SessionId).HasColumnName("SessionID");

                entity.Property(e => e.Seats)
                    .HasColumnType("varchar(max)")
                    .HasDefaultValueSql("'0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0'");

                entity.HasOne(d => d.Cineplex)
                    .WithMany(p => p.CineplexMovie)
                    .HasForeignKey(d => d.CineplexId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_CineplexMovie_Cineplex");

                entity.HasOne(d => d.Movie)
                    .WithMany(p => p.CineplexMovie)
                    .HasForeignKey(d => d.MovieId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_CineplexMovie_Movie");

                entity.HasOne(d => d.Session)
                    .WithMany(p => p.CineplexMovie)
                    .HasForeignKey(d => d.SessionId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_CineplexMovie_Session");
            });

            modelBuilder.Entity<Enquiry>(entity =>
            {
                entity.Property(e => e.EnquiryId).HasColumnName("EnquiryID");

                entity.Property(e => e.Email).IsRequired();

                entity.Property(e => e.Message).IsRequired();
            });

            modelBuilder.Entity<Movie>(entity =>
            {
                entity.Property(e => e.MovieId).HasColumnName("MovieID");

                entity.Property(e => e.ConcessionPrice)
                    .HasColumnType("decimal")
                    .HasDefaultValueSql("'20'");

                entity.Property(e => e.FullPrice)
                    .HasColumnType("decimal")
                    .HasDefaultValueSql("'45'");

                entity.Property(e => e.LongDescription).IsRequired();

                entity.Property(e => e.ShortDescription).IsRequired();

                entity.Property(e => e.Title).IsRequired();
            });

            modelBuilder.Entity<MovieComingSoon>(entity =>
            {
                entity.Property(e => e.MovieComingSoonId).HasColumnName("MovieComingSoonID");

                entity.Property(e => e.LongDescription).IsRequired();

                entity.Property(e => e.ShortDescription).IsRequired();

                entity.Property(e => e.Title).IsRequired();
            });

            modelBuilder.Entity<Session>(entity =>
            {
                entity.Property(e => e.SessionId)
                    .HasColumnName("SessionID")
                    .ValueGeneratedNever();
            });
        }

        public virtual DbSet<Booking> Booking { get; set; }
        public virtual DbSet<Cineplex> Cineplex { get; set; }
        public virtual DbSet<CineplexMovie> CineplexMovie { get; set; }
        public virtual DbSet<Enquiry> Enquiry { get; set; }
        public virtual DbSet<Movie> Movie { get; set; }
        public virtual DbSet<MovieComingSoon> MovieComingSoon { get; set; }
        public virtual DbSet<Session> Session { get; set; }
    }
}