﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Tute {
    class JsonTest {
        public JsonTest() {
            Product cat = new Product(1, "Cat", 10);
            Console.WriteLine(JsonConvert.SerializeObject(cat));
            
            List<Product> products = new List<Product> { cat, new Product(2, "Dog", 5) };
            string serializedProducts = JsonConvert.SerializeObject(products);
            Console.WriteLine(serializedProducts);
            List<Product> deserializedProducts = JsonConvert.DeserializeObject<List<Product>>(serializedProducts);
            Console.WriteLine(deserializedProducts.GetType());
            Console.WriteLine(JsonConvert.SerializeObject(deserializedProducts));
            Console.ReadKey();
        }
    }

    class Product {
        private int _id;
        private string _name;
        private int _amount;

        public Product(int id, string name, int amount) {
            _id = id;
            _name = name;
            _amount = amount;
        }

        public int id { get { return _id; } set { _id = value; } }
        public string name { get { return _name; } set { _name = value; } }
        public int amount { get { return _amount; } set { _amount = value; } }
    }
}
