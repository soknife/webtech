using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;

namespace WebTechAssignment1 {
    class JsonEditor<T> {
        string _jsonFilePath;
        
        public JsonEditor(string jsonFilePath) {
            if (!File.Exists(jsonFilePath))
                File.Create(jsonFilePath).Close();
            _jsonFilePath = jsonFilePath;
        }

        public void Write(T deserializedObject) {
            string serializedJson = JsonConvert.SerializeObject(deserializedObject);
            File.WriteAllText(_jsonFilePath, serializedJson);
        }

        public T Read() {
            try {
                string serializedJson = File.ReadAllText(_jsonFilePath);
                return JsonConvert.DeserializeObject<T>(serializedJson);
            } catch (Exception) {
                return default(T);
            }
        }
    }
}
