using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebTechAssignment1 {
    // Data paths for various Json files
    static class DataUtil {
        public const string requestJson = "./stock_request.json";
        public const string ownerInventoryJson = "./owner_inventory.json";
        public static string FranchiseOwnerJson(string store) {
            return "./" + store + "_inventory.json";
        }
    }
}
