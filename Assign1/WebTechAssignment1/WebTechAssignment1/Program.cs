using System;
using System.Collections.Generic;
using WebTechAssignment1.Menus;
using WebTechAssignment1.Menus.FranchiseMenu;
using WebTechAssignment1.Menus.CustomerMenu;
using WebTechAssignment1.Menus.OwnerMenu;
using WebTechAssignment1.DataStructures;
using System.IO;

namespace WebTechAssignment1 {
    class Program {
        static void Main(string[] args) {
            Console.WindowWidth = 100;
            Console.WriteLine("Magic Inventory System 1.0");

            MenuOption ownerOption = new OwnerMenuOption();
            MenuOption franchiseOption = new FranchiseOwnerMenuOption();
            MenuOption customerOption = new CustomerMenuOption();
            Menu mainMenu = new Menu("Welcome to Marvellous Magic", ownerOption, franchiseOption, customerOption);
            mainMenu.Run(false);
            
        }
    }
}
