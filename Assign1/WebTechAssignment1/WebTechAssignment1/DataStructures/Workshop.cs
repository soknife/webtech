using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebTechAssignment1.DataStructures {
    // Workshop session times
    enum workshopTime {
        Morning,
        Afternoon
    }
    
    // Stores and manages a workshop session
    class Workshop {
        public List<string> _bookings;
        public int _capacity;

        public Workshop(int capacity) {
            _bookings = new List<string>();
            _capacity = capacity;
        }

        public bool AddBooking(string reference) {
            if (_bookings.Count < _capacity - 1) {
                _bookings.Add(reference);
                return true;
            } else {
                return false;
            }
        }

        public int availableSpace { get { return _capacity - _bookings.Count; } }
    }
}
