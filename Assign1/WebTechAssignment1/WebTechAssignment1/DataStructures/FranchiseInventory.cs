using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebTechAssignment1.DataStructures {
    // Franchise specific inventory with additional workshop data
    class FranchiseInventory : Inventory {
        private Dictionary<workshopTime, Workshop> _workshops;

        public FranchiseInventory() : base() {
            _workshops = new Dictionary<workshopTime, Workshop>();

            // Temporay definition for capacity as this is an non-essential feature right now
            _workshops.Add(workshopTime.Morning, new Workshop(10));
            _workshops.Add(workshopTime.Afternoon, new Workshop(5));
        }

        public Dictionary<workshopTime, Workshop> workshops { get { return _workshops; } }
    }
}
