using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebTechAssignment1 {
    // A request composed of the requesting store and requested product
    class Request : Product {
        private string _store;

        public Request(string name, uint amount, string store ) : base(name, amount) {
            _store = store;
        }

        public string store { get { return _store; } set { _store = store; } }
    }
}
