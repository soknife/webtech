using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebTechAssignment1.DataStructures {
    // Stores products and workshop bookings for a customer
    class ShoppingCartInventory : Inventory {
        private string _workshopBooking;

        public ShoppingCartInventory() : base() {
        }

        public void ResetCart() {
            _workshopBooking = null;
            base.products.Clear();
        }

        public string workshopBooking { set { _workshopBooking = value; } get { return _workshopBooking; } }
    }
}
