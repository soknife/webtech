using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebTechAssignment1.DataStructures {
    // Store and manages products
    class Inventory {
        private List<Product> _products;

        public Inventory() {
            _products = new List<Product>();
        }

        // Add a product, by quantity if it is existing, add new if it is non-existing
        public void AddProduct(Product product, uint quantity) {
            Product existingProduct = _products.Find(x => x.name == product.name);
            if (existingProduct == null) {
                existingProduct = new Product(product.name, quantity);
                _products.Add(existingProduct);
                return;
            }
            existingProduct.quantity += quantity;
        }

        public void AddProduct(Product product) {
            AddProduct(product, product.quantity);
        }

        public void RemoveProduct(Product product) {
            _products.RemoveAll(x => x.name == product.name);
        }

        // Remove a certian amount of a product, unless it goes into the negative
        public bool SubtractProduct(Product product, uint quantity) {
            Product existingProduct = _products.Find(x => x.name == product.name);
            if (existingProduct == null) {
                return false;
            } else if (existingProduct.quantity < quantity) {
                return false;
            }
            existingProduct.quantity -= quantity;
            return true;
        }

        public bool SubtractProduct(Request request) {
            return SubtractProduct(request, request.quantity);
        }

        public Product Find(Product product) {
            return products.Find(x => x.name == product.name);
        }

        public int FindIndex(Product product) {
            return products.FindIndex(x => x.name == product.name);
        }

        public List<Product> products { get { return _products; } }
    }
}
