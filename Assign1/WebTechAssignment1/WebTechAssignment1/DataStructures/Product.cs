using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebTechAssignment1 {
    class Product {
        private string _name;
        private uint _quantity;

        public Product(string name, uint quantity) {
            _name = name;
            _quantity = quantity;
        }
        
        public string name { get { return _name; } set { _name = value; } }
        public uint quantity { get { return _quantity; } set { _quantity = value; } }
    }
}
