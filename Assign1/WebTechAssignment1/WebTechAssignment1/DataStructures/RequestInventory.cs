using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebTechAssignment1.DataStructures {
    // Stores and manages requests
    class RequestInventory {
        private List<Request> _requests = new List<Request>();

        public RequestInventory() {
            _requests = new List<Request>();
        }

        // Add a request, overwrite if existing
        public void AddRequest(Request request) {
            Request existingRequest = Find(request);
            if (existingRequest == null) {
                _requests.Add(request);
            } else {
                existingRequest.quantity = request.quantity;
            }
        }

        // Add a request, overwrite if existing
        public void AddRequest(Product product, uint quantity, string store) {
            Request existingRequest = _requests.Find(x => x.name == product.name && x.store == store);
            if (existingRequest == null) {
                existingRequest = new Request(product.name, quantity, store);
                _requests.Add(existingRequest);
                return;
            } else {
                existingRequest.quantity = quantity;
            }
        }

        public void RemoveRequest(Product product, string store) {
            _requests.RemoveAll(x => x.name == product.name && x.store == store);
        }

        public void RemoveRequest(Request request) {
            _requests.RemoveAll(x => x.name == request.name && x.store == request.store);
        }

        public Request Find(Request request) {
            return requests.Find(x => x.name == request.name && x.store == request.store);
        }

        public int FindIndex(Request request) {
            return requests.FindIndex(x => x.name == request.name && x.store == request.store);
        }

        public List<Request> requests { get { return _requests; } }

    }
}
