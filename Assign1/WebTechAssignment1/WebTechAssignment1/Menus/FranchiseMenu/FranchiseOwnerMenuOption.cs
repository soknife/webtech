using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace WebTechAssignment1.Menus.FranchiseMenu {
    // Menu displayed to a franchisee, requires franchise name before displaying options
    class FranchiseOwnerMenuOption : FranchiseMenuOption {
        public FranchiseOwnerMenuOption() {
            base.name = "Franchise Owner";
        }

        public override void Run() {
            // User input for franchise name
            string franchiseName = base.getFranchiseName();
            if (franchiseName == null)
                return;

            MenuOption displayInventory = new DisplayInventory(franchiseName);
            MenuOption displayInventoryThreshold = new DisplayInventoryThreshold(franchiseName);
            MenuOption addNewInventoryItem = new AddNewInventoryItem(franchiseName);
            Menu franchiseMenu = new Menu("Welcome to Marvellous Magic (Franchise Holder - " + franchiseName +")",
                displayInventory, displayInventoryThreshold, addNewInventoryItem);
            franchiseMenu.Run(true);
        }
    }
}
