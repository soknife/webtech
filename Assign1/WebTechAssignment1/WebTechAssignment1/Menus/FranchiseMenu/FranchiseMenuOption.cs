using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace WebTechAssignment1.Menus.FranchiseMenu {
    // The base class for all parent menus that require a franchise name to inputted
    abstract class FranchiseMenuOption : MenuOption {
        public override abstract void Run();

        // Gets franchise name from input
        protected string getFranchiseName() {
            // User input
            string franchiseName;
            while (true) {
                Console.Write("Enter store name, or R to return: ");
                string input = Console.ReadLine().Trim();
                if (Regex.IsMatch(input, "(?i)r"))
                    return null;
                
                // Check valid input
                bool invalidInput = false;
                try {
                    if (!File.Exists(DataUtil.FranchiseOwnerJson(input)))
                        invalidInput = true;
                }
                catch (Exception) {
                    invalidInput = true;
                }
                if (invalidInput) {
                    Console.WriteLine("Invalid option");
                    continue;
                }
                else {
                    franchiseName = input;
                    return franchiseName;
                }
            }
        }
    }
}
