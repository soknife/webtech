using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using WebTechAssignment1.DataStructures;

namespace WebTechAssignment1.Menus.FranchiseMenu {
    class DisplayInventory : FranchiseChildMenuOption {
        protected string _format = "{0,2} {1,20} {2,15} {3,10}";

        public DisplayInventory(string franchiseName) : base (franchiseName) {
            base.name = "Display Inventory";
        }

        // Get product details
        public virtual string ProductText(int id, Product product, uint threshold) {
            string name = product.name;
            uint quantity = product.quantity;
            bool restock = (quantity < threshold);
            return String.Format(_format, id, name, quantity, restock);
        }

        // Validates any product for request
        public virtual bool ValidateProduct(Product product, uint threshold) {
            return true;
        }

        public override void Run() {
            // User input for threshold
            uint threshold;
            while (true) {
                Console.Write("Enter threshold for re-stock, or R to return: ");
                string input = Console.ReadLine().Trim();
                if (Regex.IsMatch(input, "(?i)r")) {
                    return;
                }
                uint option = 1;
                bool invalidInput = false;
                // Check valid input
                try {
                    option = Convert.ToUInt32(input);
                    if (option < 1)
                        invalidInput = true;
                } catch (Exception) {
                    invalidInput = true;
                }
                if (invalidInput) {
                    Console.WriteLine("Invalid option");
                    continue;
                } else {
                    threshold = option;
                    break;
                }
            }
            
            // Request display and processing
            while (true) {
                Console.Clear();

                // Read data
                JsonEditor<FranchiseInventory> franchiseInventoryJson = new JsonEditor<FranchiseInventory>(DataUtil.FranchiseOwnerJson(_franchiseName));
                FranchiseInventory franchiseInventory = franchiseInventoryJson.Read();
                if (franchiseInventory == null)
                    franchiseInventory = new FranchiseInventory();
                
                // Display requests
                Console.WriteLine("Inventory");
                Console.WriteLine(_format, "ID", "Product", "Current Stock", "Re-Stock");
                Console.WriteLine("__________________________________________________");
                for (int index = 0; index < franchiseInventory.products.Count; index++) {
                    string productText = ProductText(index + 1, franchiseInventory.products[index], threshold);
                    if (productText != null)
                        Console.WriteLine(productText);
                }

                // User input for request
                while (true) {
                    Console.Write("Enter product to request, or R to return: ");
                    string input = Console.ReadLine().Trim();
                    if (Regex.IsMatch(input, "(?i)r")) {
                        return;
                    }
                    int option = 1;
                    bool invalidInput = false;
                    // Check valid input
                    try {
                        option = Convert.ToInt32(input);
                        if (option < 1 || option > franchiseInventory.products.Count || !ValidateProduct(franchiseInventory.products[option - 1], threshold))
                            invalidInput = true;
                    } catch (Exception) {
                        invalidInput = true;
                    }
                    if (invalidInput) {
                        Console.WriteLine("Invalid option");
                        continue;
                    }

                    // User input for ignoring the threshold if threshold is not reached
                    bool abandonRequest = false;
                    while (franchiseInventory.products[option - 1].quantity >= threshold) {
                        Console.Write("Product quantity is sufficient, continue with request? (Yes/No): ");
                        input = Console.ReadLine().Trim();
                        if (Regex.IsMatch(input, "(?i)yes")) {
                            break;
                        } else if (Regex.IsMatch(input, "(?i)no")) {
                            abandonRequest = true;
                            break;
                        } else {
                            Console.WriteLine("Invalid option");
                            continue;
                        }
                    }
                    if (abandonRequest)
                        continue;

                    // Write request
                    JsonEditor<RequestInventory> requestJson = new JsonEditor<RequestInventory>(DataUtil.requestJson);
                    RequestInventory requestInventory = requestJson.Read();
                    if (requestInventory == null)
                        requestInventory = new RequestInventory();
                    Product requestedProduct = franchiseInventory.products[option - 1];
                    requestInventory.AddRequest(requestedProduct, threshold - requestedProduct.quantity, _franchiseName);
                    requestJson.Write(requestInventory);
                    break;
                }
            }
        }
    }
}
