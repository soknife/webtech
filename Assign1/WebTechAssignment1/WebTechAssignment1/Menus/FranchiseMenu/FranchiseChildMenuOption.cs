﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebTechAssignment1.Menus.FranchiseMenu {
    // Base class for all child menus that require a franchise name from a parent menu
    abstract class FranchiseChildMenuOption : MenuOption {
        protected string _franchiseName;

        public FranchiseChildMenuOption(string franchiseName) {
            _franchiseName = franchiseName;
        }

        public override abstract void Run();
    }
}
