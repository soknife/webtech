using System;
using System.Text.RegularExpressions;
using WebTechAssignment1.DataStructures;

namespace WebTechAssignment1.Menus.FranchiseMenu {
    class AddNewInventoryItem : FranchiseChildMenuOption {
        private string _format = "{0,2} {1,20} {2,15}";

        public AddNewInventoryItem(string franchiseName) : base(franchiseName) {
            base.name = "Add New Inventory Item";
        }

        public override void Run() {
            while (true) {
                Console.Clear();

                // Read data
                JsonEditor<FranchiseInventory> franchiseInventoryJson = new JsonEditor<FranchiseInventory>(DataUtil.FranchiseOwnerJson(_franchiseName));
                FranchiseInventory franchiseInventory = franchiseInventoryJson.Read();
                if (franchiseInventory == null)
                    franchiseInventory = new FranchiseInventory();
                JsonEditor<Inventory> ownerInvenotoryJson = new JsonEditor<Inventory>(DataUtil.ownerInventoryJson);
                Inventory ownerInventory = ownerInvenotoryJson.Read();
                if (ownerInventory == null)
                    ownerInventory = new Inventory();

                // Display data
                Console.WriteLine("Available products");
                Console.WriteLine(_format, "ID", "Product", "Quantity");
                Console.WriteLine("_______________________________________");
                for (int index = 0; index < ownerInventory.products.Count; index++) {
                    if (franchiseInventory.Find(ownerInventory.products[index]) != null)
                        continue;
                    string product = ownerInventory.products[index].name;
                    uint quantity = ownerInventory.products[index].quantity;
                    Console.WriteLine(_format, index + 1, product, quantity);
                }

                // User input for adding a product
                while (true) {
                    // Choose product to add
                    Console.Write("Enter product to request, or R to return: ");
                    string input = Console.ReadLine().Trim();
                    if (Regex.IsMatch(input, "(?i)r")) {
                        return;
                    }
                    int option = 1;
                    bool invalidInput = false;
                    // Check valid input
                    try {
                        option = Convert.ToInt32(input);
                        if (option < 1 || franchiseInventory.Find(ownerInventory.products[option - 1]) != null)
                            invalidInput = true;
                    } catch (Exception) {
                        invalidInput = true;
                    }
                    if (invalidInput) {
                        Console.WriteLine("Invalid option");
                        continue;
                    }

                    // Choose amount to add
                    Console.Write("Enter amount to request, or R to return: ");
                    input = Console.ReadLine().Trim();
                    if (Regex.IsMatch(input, "(?i)r")) {
                        return;
                    }
                    uint amount = 1;
                    invalidInput = false;
                    // Check valid input
                    try {
                        amount = Convert.ToUInt32(input);
                        if (option < 1)
                            invalidInput = true;
                    } catch (Exception) {
                        invalidInput = true;
                    }
                    if (invalidInput) {
                        Console.WriteLine("Invalid option");
                        continue;
                    }
                    
                    JsonEditor<RequestInventory> requestJson = new JsonEditor<RequestInventory>(DataUtil.requestJson);
                    RequestInventory requestInventory = requestJson.Read();
                    if (requestInventory == null)
                        requestInventory = new RequestInventory();

                    // Request does not exist, simply add request
                    Product requestedProduct = ownerInventory.products[option - 1];
                    Request request = new Request(requestedProduct.name, amount, _franchiseName);
                    if (requestInventory.Find(request) == null) {
                        requestInventory.AddRequest(request);
                    }
                    // Request already exists, ask for confirmation
                    else  {
                        bool overwrite = false;
                        while (true) {
                            Console.Write("Request already exists, overwrite existing? (Yes/No), or R to return: ");
                            input = Console.ReadLine().Trim();
                            if (Regex.IsMatch(input, "(?i)r")) {
                                return;
                            }
                            // Validate input
                            try {
                                if (Regex.IsMatch(input, "(?i)y|yes"))
                                    overwrite = true;
                                else if (Regex.IsMatch(input, "(?i)n|no"))
                                    overwrite = false;
                                else
                                    invalidInput = true;
                            } catch (Exception) {
                                invalidInput = true;
                            }
                            if (invalidInput) {
                                Console.WriteLine("Invalid option");
                                continue;
                            } else {
                                if (overwrite)
                                    requestInventory.AddRequest(request);
                                break;
                            }
                        }
                    }

                    // Write request
                    requestJson.Write(requestInventory);
                    break;
                }
            }
        }
    }
}
