using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTechAssignment1.DataStructures;

namespace WebTechAssignment1.Menus.FranchiseMenu {
    class DisplayInventoryThreshold : DisplayInventory {

        public DisplayInventoryThreshold(string franchiseName) : base (franchiseName) {
            base.name = "Display Inventory (Threshold)";
        }

        // Get product details if passes threshold check
        public override string ProductText(int id, Product product, uint threshld) {
            string name = product.name;
            uint quantity = product.quantity;
            bool restock = (quantity < threshld);
            if (restock)
                return String.Format(base._format, id, name, quantity, restock);
            else
                return null;
        }

        // Only validate a product for request if it passes threshold check
        public override bool ValidateProduct(Product product, uint threshold) {
            return (product.quantity < threshold);
        }

        public override void Run() {
            base.Run(); // Runs the base DisplayInventory.run() method
        }
    }
}
