using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using WebTechAssignment1.DataStructures;

namespace WebTechAssignment1.Menus.OwnerMenu {
    class DisplayAllStockRequests : MenuOption {
        protected string _format = "{0,2} {1,10} {2,20} {3,10} {4,15} {5,20}";

        public DisplayAllStockRequests() {
            base.name = "Display All Stock Requests";
        }

        // Details for a request
        public virtual string RequestText(int id, Request request, Inventory ownerInventory) {
            string store = request.store;
            string product = request.name;
            uint quantity = request.quantity;
            uint currentStock;
            bool availability;
            Product ownerProduct = ownerInventory.Find(request);
            if (ownerProduct != null) {
                currentStock = ownerProduct.quantity;
                availability = (currentStock >= quantity);
            } else {
                currentStock = 0;
                availability = false;
            }
            return String.Format(_format, id, store, product, quantity, currentStock, availability);
        }

        // Check validity if request exists
        public virtual bool ValidateRequest(Request request, Inventory ownerInventory) {
            return ownerInventory.Find(request) != null;
        }

        public override void Run() {
            Console.Clear();

            while (true) {
                // Read data
                JsonEditor<RequestInventory> requestJson = new JsonEditor<RequestInventory>(DataUtil.requestJson);
                RequestInventory requestInventory = requestJson.Read();
                if (requestInventory == null)
                    requestInventory = new RequestInventory();
                JsonEditor<Inventory> ownerInvenotoryJson = new JsonEditor<Inventory>(DataUtil.ownerInventoryJson);
                Inventory ownerInventory = ownerInvenotoryJson.Read();
                if (ownerInventory == null)
                    ownerInventory = new Inventory();

                // Display requests
                Console.WriteLine("Stock Requests");
                Console.WriteLine(_format, "ID", "Store", "Product", "Quantity", "Current Stock", "Stock Availability");
                Console.WriteLine("__________________________________________________________________________________");
                for (int index = 0; index < requestInventory.requests.Count; index++) {
                    string requestText = RequestText(index + 1, requestInventory.requests[index], ownerInventory);
                    if (requestText != null)
                        Console.WriteLine(requestText);
                }

                // User input
                while (true) {
                    Console.Write("Enter Request to process, or R to return: ");
                    string input = Console.ReadLine().Trim();
                    if (Regex.IsMatch(input, "(?i)r")) {
                        return;
                    }
                    int option = 1;
                    bool invalidInput = false;
                    // Check valid input
                    try {
                        option = Convert.ToInt32(input);
                        if (option < 1 || !ValidateRequest(requestInventory.requests[option - 1], ownerInventory))
                            invalidInput = true;
                    } catch (Exception) {
                        invalidInput = true;
                    }
                    if (invalidInput) {
                        Console.WriteLine("Invalid option");
                        continue;
                    }

                    // Check sufficient stock
                    Request currentRequest = requestInventory.requests[option - 1];
                    int ownerProductIndex = ownerInventory.FindIndex(currentRequest);
                    if (ownerProductIndex == -1) {
                        Console.WriteLine("Insufficient stock to fulfill the request");
                        continue;
                    }
                    if (!ownerInventory.SubtractProduct(currentRequest)) {
                        Console.WriteLine("Insufficient stock to fulfill the request");
                        continue;
                    }
                    // Remove product quantity from owner 
                    ownerInvenotoryJson.Write(ownerInventory);

                    // Add product quantity to franchise
                    JsonEditor<FranchiseInventory> franchiseInvenotoryJson = new JsonEditor<FranchiseInventory>(
                        DataUtil.FranchiseOwnerJson(requestInventory.requests[option -1].store));
                    FranchiseInventory franchiseInventory = franchiseInvenotoryJson.Read();
                    if (franchiseInventory == null)
                        franchiseInventory = new FranchiseInventory();
                    franchiseInventory.AddProduct(currentRequest, currentRequest.quantity);
                    franchiseInvenotoryJson.Write(franchiseInventory);

                    // Remove request
                    requestInventory.RemoveRequest(currentRequest);
                    requestJson.Write(requestInventory);

                    Console.Clear();
                    break;
                }
            }
        }
    }
}
