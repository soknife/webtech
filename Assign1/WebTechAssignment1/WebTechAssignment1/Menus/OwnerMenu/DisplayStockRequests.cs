using System;
using System.Text.RegularExpressions;
using WebTechAssignment1.DataStructures;

namespace WebTechAssignment1.Menus.OwnerMenu {
    class DisplayStockRequests : DisplayAllStockRequests {
        private bool availabilityFilter;

        public DisplayStockRequests() {
            base.name = "Display Stock Requests (True/False)";
        }

        // Details for a request, filtered by an availability option
        public override string RequestText(int id, Request request, Inventory ownerInventory) {
            string store = request.store;
            string product = request.name;
            uint quantity = request.quantity;
            uint currentStock;
            bool availability;
            Product ownerProduct = ownerInventory.Find(request);
            if (ownerProduct != null) {
                currentStock = ownerProduct.quantity;
                availability = (currentStock >= quantity);
            } else {
                currentStock = 0;
                availability = false;
            }
            if (availability == availabilityFilter)
                return String.Format(_format, id, store, product, quantity, currentStock, availability);
            else
                return null;
        }

        // Check validity of request that it exists and matches filter
        public override bool ValidateRequest(Request request, Inventory ownerInventory) {
            Product ownerProduct = ownerInventory.Find(request);
            if (ownerProduct != null) {
                return (ownerProduct.quantity >= request.quantity) == availabilityFilter;
            } else {
                return !availabilityFilter;
            }
        }

        public override void Run() {
            // User input
            while (true) {
                Console.Write("Enter availability filter (True/False), or R to return: ");
                string input = Console.ReadLine().Trim();
                if (Regex.IsMatch(input, "(?i)r")) {
                    return;
                }
                bool invalidInput = false;
                bool option = true;
                // Validate input
                try {
                    if (Regex.IsMatch(input, "(?i)t|true"))
                        option = true;
                    else if (Regex.IsMatch(input, "(?i)f|false"))
                        option = false;
                    else
                        invalidInput = true;
                } catch (Exception) {
                    invalidInput = true;
                }
                if (invalidInput) {
                    Console.WriteLine("Invalid option");
                    continue;
                } else {
                    availabilityFilter = option;

                    // Run the same process as DisplayAllStockRequestMenuOption.Run()
                    base.Run();
                    return;
                }
            }
        }
    }
}
