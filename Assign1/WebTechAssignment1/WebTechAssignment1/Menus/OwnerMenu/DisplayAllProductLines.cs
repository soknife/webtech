using System;
using WebTechAssignment1.DataStructures;

namespace WebTechAssignment1.Menus.OwnerMenu {
    class DisplayAllProductLines : MenuOption {
        private string _format = "{0,2} {1,20} {2,15}";

        public DisplayAllProductLines() {
            base.name = "Display All Product Lines";
        }

        public override void Run() {
            Console.Clear();

            // Read data
            JsonEditor<Inventory> ownerInvenotoryJson = new JsonEditor<Inventory>(DataUtil.ownerInventoryJson);
            Inventory ownerInventory = ownerInvenotoryJson.Read();
            if (ownerInventory == null)
                ownerInventory = new Inventory();

            // Display data
            Console.WriteLine("Inventory");
            Console.WriteLine(_format, "ID", "Product", "Quantity");
            Console.WriteLine("_______________________________________");
            for (int index = 0; index < ownerInventory.products.Count; index++) {
                string product = ownerInventory.products[index].name;
                uint quantity = ownerInventory.products[index].quantity;
                Console.WriteLine(_format, index + 1, product, quantity);
            }

            Console.WriteLine("Press any key to continue...");
            Console.ReadKey(false);
        }
    }
}
