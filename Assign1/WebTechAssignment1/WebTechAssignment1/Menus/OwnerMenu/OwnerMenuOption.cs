using System;

namespace WebTechAssignment1.Menus.OwnerMenu {
    // Menu displayed to the owner
    class OwnerMenuOption : MenuOption {

        public OwnerMenuOption() {
            base.name = "Owner";
        }

        public override void Run() {
            MenuOption displayAllStockRequest = new DisplayAllStockRequests();
            MenuOption displayStockRequest = new DisplayStockRequests();
            MenuOption displayAllProductLines = new DisplayAllProductLines();
            Menu ownerMenu = new Menu("Welcome to Marvellous Magic (Owner)",
                displayAllStockRequest, displayStockRequest, displayAllProductLines);
            ownerMenu.Run(true);
        }
    }
}
