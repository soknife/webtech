using System;
using WebTechAssignment1.DataStructures;
using WebTechAssignment1.Menus.FranchiseMenu;

namespace WebTechAssignment1.Menus.CustomerMenu {
    // Menu displayed to customer, requires a franchise name
    class CustomerMenuOption : FranchiseMenuOption {

        public CustomerMenuOption() {
            base.name = "Customer";
        }

        public override void Run() {
            ShoppingCartInventory shoppingCart = new ShoppingCartInventory(); // Current shopping card

            string franchiseName = base.getFranchiseName();
            JsonEditor<FranchiseInventory> franchiseInventoryJson = new JsonEditor<FranchiseInventory>(DataUtil.FranchiseOwnerJson(franchiseName));
            FranchiseInventory franchiseInventory = franchiseInventoryJson.Read();
            if (franchiseInventory == null)
                franchiseInventory = new FranchiseInventory();

            MenuOption displayProducts = new DisplayProdcuts(franchiseName, shoppingCart, franchiseInventory);
            MenuOption displayWorkshops = new DisplayWorkshops(franchiseName, shoppingCart, franchiseInventory);
            Menu customerMenu = new Menu("Welcome to marvelous magic (Retial - " + franchiseName + ")",
                displayProducts, displayWorkshops);
            customerMenu.Run(true);
        }
    }
}
