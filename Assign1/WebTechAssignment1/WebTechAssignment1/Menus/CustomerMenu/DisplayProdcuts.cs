using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using WebTechAssignment1.DataStructures;
using WebTechAssignment1.Menus.FranchiseMenu;

namespace WebTechAssignment1.Menus.CustomerMenu {
    class DisplayProdcuts : CustomerChildMenuOption {
        private string _format = "{0,2} {1,20} {2,15}";
        private int _pageSize = 5;

        public DisplayProdcuts(string franchiseName, ShoppingCartInventory shoppingCart, FranchiseInventory inventory) : base(franchiseName, shoppingCart, inventory) {
            base.name = "Products";
        }

        public override void Run() {
            int firstDisplayProductIndex = 0; // Index of the product that is displayed as the 1st item 

            while (true) {
                Console.Clear();
                if (_inventory == null || _inventory.products.Count < 1) {
                    Console.WriteLine("No products found, press any key to continue...");
                    Console.ReadKey(false);
                    return;
                }

                // Display data
                Console.WriteLine("Available products");
                Console.WriteLine(_format, "ID", "Product", "Current Stock");
                Console.WriteLine("_______________________________________");
                for (int index = firstDisplayProductIndex; index < firstDisplayProductIndex + 5 && index < _inventory.products.Count; index++) {
                    string product = _inventory.products[index].name;
                    uint quantity = _inventory.products[index].quantity;
                    Console.WriteLine(_format, index + 1, product, quantity);
                }
                Console.WriteLine("Additional options: 'P' to next page | 'R' to return | 'C' to complete transaction");

                // User input
                while(true) {
                    // Choose product or additional option
                    Console.Write("Enter product to request, or an additional option: ");
                    string input = Console.ReadLine().Trim();
                    if (Regex.IsMatch(input, "(?i)r")) { // Return request 
                        return;
                    } else if (Regex.IsMatch(input, "(?i)p")) { // Next page request
                        if (firstDisplayProductIndex + _pageSize < _inventory.products.Count)
                            firstDisplayProductIndex += _pageSize; // Increment by 5 indicies if there are more than 5 remaining
                        else
                            firstDisplayProductIndex = 0; // Reset to first page otherwise
                        break;
                    } else if (Regex.IsMatch(input, "(?i)c")) { // Complete transaction request
                        base.ProcessTransaction();
                        return;
                    } 
                    // Product selection or invalid input
                    int option = 1;
                    bool invalidInput = false;
                    // Check valid input
                    try {
                        option = Convert.ToInt32(input);
                        if (option < 1 || option > _inventory.products.Count)
                            invalidInput = true;
                    }
                    catch (Exception) {
                        invalidInput = true;
                    }
                    if (invalidInput) {
                        Console.WriteLine("Invalid option");
                        continue;
                    }
                    
                    // Choose quantity
                    Console.Write("Enter amount to request, or 'R' to return: ");
                    input = Console.ReadLine().Trim();
                    if (Regex.IsMatch(input, "(?i)r")) { // Return request 
                        return;
                    }
                    // Quantity selection or invalid input
                    int amount = 0;
                    invalidInput = false;
                    // Check valid input
                    try {
                        amount = Convert.ToInt32(input);
                        if (amount < 1 || amount > _inventory.products[option - 1].quantity)
                            invalidInput = true;
                    } catch (Exception) {
                        invalidInput = true;
                    }
                    if (invalidInput) {
                        Console.WriteLine("Invalid option");
                        continue;
                    }
                    
                    // Remove product from franchise inventory
                    Product product = _inventory.products[option - 1];
                    if (_inventory.SubtractProduct(product, (uint)amount)) {
                        // Add product to cart
                        _shoppingCart.AddProduct(product, (uint)amount);
                    }
                    break;
                }

                
            }
        }
    }
}
