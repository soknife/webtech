using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTechAssignment1.DataStructures;
using WebTechAssignment1.Menus.FranchiseMenu;

namespace WebTechAssignment1.Menus.CustomerMenu {
    abstract class CustomerChildMenuOption : FranchiseChildMenuOption {
        protected ShoppingCartInventory _shoppingCart;
        protected FranchiseInventory _inventory;
        private string _format = "{0,20} {1,10}";

        public CustomerChildMenuOption(string franchiseName, ShoppingCartInventory shoppingCart, FranchiseInventory inventory) : base(franchiseName) {
            _shoppingCart = shoppingCart;
            _inventory = inventory;
        }

        // Displays the transaction summary, and writes data to json
        protected void ProcessTransaction() {
            Console.Clear();
            Console.WriteLine("Transaction summary:");

            // Display products
            Console.WriteLine(_format, "Product", "Quantity");
            Console.WriteLine("____________________________________");
            foreach (Product cartItem in _shoppingCart.products) {
                Console.WriteLine(_format, cartItem.name, cartItem.quantity);
            }

            // Display workshop bookings
            if (this._shoppingCart.workshopBooking != null) {
                Console.WriteLine("Workshop booking: ");
                Console.WriteLine(this._shoppingCart.workshopBooking);
                Console.WriteLine("You recerved a 10% discount due to your workshop booking");
            }

            // Write changes to json
            JsonEditor<FranchiseInventory> franchiseInventoryJson = new JsonEditor<FranchiseInventory>(DataUtil.FranchiseOwnerJson(_franchiseName));
            franchiseInventoryJson.Write(_inventory);
            _shoppingCart.ResetCart();

            Console.WriteLine("Transaction complete, thank you for your pateronage");
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey(false);
        }

        public abstract override void Run();
    }
}
