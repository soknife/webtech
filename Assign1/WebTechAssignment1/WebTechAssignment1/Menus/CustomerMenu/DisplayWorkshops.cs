using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using WebTechAssignment1.DataStructures;
using WebTechAssignment1.Menus.FranchiseMenu;

namespace WebTechAssignment1.Menus.CustomerMenu {
    class DisplayWorkshops : CustomerChildMenuOption {
        string _format = "{0,2} {1,11} {2,11}";

        public DisplayWorkshops(string franchiseName, ShoppingCartInventory shoppingCart, FranchiseInventory inventory) : base(franchiseName, shoppingCart, inventory) {
            base.name = "Workshops";
        }

        // Generates a unique number based on a precise current time
        private string GenerateUniqueReference() {
            return DateTime.Now.ToString("yyyyMMddHHmmssffff");
        }

        public override void Run() {
            while (true) {
                Console.Clear();

                // Display workshops 
                Console.WriteLine("Current workshops");
                Console.WriteLine(_format, "ID", "Time", "Available");
                Console.WriteLine("__________________________");
                for (int index = 0; index < _inventory.workshops.Count; index++) {
                    string session = _inventory.workshops.ElementAt(index).Key.ToString();
                    int available = _inventory.workshops.ElementAt(index).Value.availableSpace;
                    Console.WriteLine(_format, index + 1, session, available);
                }
                Console.WriteLine("Additional options: 'R' to return | 'C' to complete transaction");

                // User input
                while (true) {
                    Console.Write("Enter workshop to request, or an additional option: ");
                    string input = Console.ReadLine().Trim();
                    if (Regex.IsMatch(input, "(?i)r")) { // Return request
                        return;
                    } else if (Regex.IsMatch(input, "(?i)c")) { // Complete transaction request
                        base.ProcessTransaction();
                        return;
                    }
                    // Workshop select request or invalid input
                    else { 
                        int option = 1;
                        bool invalidInput = false;
                        // Check valid input
                        try {
                            option = Convert.ToInt32(input);
                            if (option < 1 || option > _inventory.workshops.Count)
                                invalidInput = true;
                        } catch (Exception) {
                            invalidInput = true;
                        }
                        if (invalidInput) {
                            Console.WriteLine("Invalid option");
                            continue;
                        }

                        // Ignore if user has already booked session
                        if (_shoppingCart.workshopBooking != null) {
                            Console.WriteLine("You have already booked a workshop session");
                            continue;
                        }

                        // Attempt to add session
                        string reference = GenerateUniqueReference();
                        KeyValuePair<workshopTime, Workshop> workshopSession = _inventory.workshops.ElementAt(option - 1);
                        if (!workshopSession.Value.AddBooking(reference)) { // Session full, simply ignore
                            Console.WriteLine("Workshop is full, unable to create a booking");
                            continue;
                        } else { // Add session
                            _shoppingCart.workshopBooking = 
                                " Store: " + _franchiseName + 
                                " Session: " + workshopSession.Key.ToString() + 
                                " Reference: " + reference;
                            continue;
                        }
                    }
                }
            }
        }
    }
}
