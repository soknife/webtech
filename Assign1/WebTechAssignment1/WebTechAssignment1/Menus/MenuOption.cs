
namespace WebTechAssignment1.Menus {
    // Represents an available option inside any menu
    abstract class MenuOption : IMenuOption {
        private string _name;

        public MenuOption() {
        }

        // Working body of a given menu
        abstract public void Run();

        public string name { get { return _name; } protected set { _name = value; } }
    }
}
