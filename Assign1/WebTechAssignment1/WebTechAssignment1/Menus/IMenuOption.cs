using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebTechAssignment1 {
    interface IMenuOption {
        void Run();
    }
}
