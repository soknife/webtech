using System;
using System.Collections.Generic;
using WebTechAssignment1.Menus;

namespace WebTechAssignment1 {
    // Displays a list of menu options and accept user input repeated to execute any of those options
    class Menu {
        protected string _title;
        List<MenuOption> _menus;

        public Menu(string title, params MenuOption[] menus) {
            _title = title;
            _menus = new List<MenuOption>(menus);
        }

        // Displays all option available, and repeatedly accept inputs
        public void Run(bool isChildMenu) {
            Console.Clear();

            while (true) {
                // Display menu options, Return option available when child menu, Quit option always available
                Console.WriteLine(_title);
                Console.WriteLine("======================================================================");
                for (int menuIndex = 0; menuIndex <= _menus.Count; menuIndex++) {
                    if (menuIndex == _menus.Count) {
                        if (isChildMenu) // Return option available when this menu is a child
                            Console.WriteLine("\t{0}. Return to previous menu", ++menuIndex);
                        Console.WriteLine("\t{0}. Quit", ++menuIndex);
                        break;
                    }
                    Console.WriteLine("\t{0}. {1}", menuIndex + 1, _menus[menuIndex].name);
                }

                // User input
                while (true) {
                    // Prompt for input
                    int additionalOption = 1;
                    if (isChildMenu)
                        additionalOption++;
                    Console.Write("Please enter an option (1-{0}): ", _menus.Count + additionalOption);

                    // Read user input
                    string input = Console.ReadLine().Trim();
                    int option = 1;
                    bool invalidInput = false;
                    try {
                        option = Convert.ToInt32(input);
                        if (option < 1) {
                            invalidInput = true;
                        } else {
                            if (isChildMenu) {
                                if (option > _menus.Count + 2)
                                    invalidInput = true;
                            } else {
                                if (option > _menus.Count + 1)
                                    invalidInput = true;
                            }
                        }

                    } catch (Exception) {
                        invalidInput = true;
                    }
                    if (invalidInput) {
                        Console.WriteLine("Invalid option");
                        continue;
                    }

                    //Run menu option
                    if (isChildMenu) {
                        if (option == _menus.Count + 1)
                            return;
                        else if (option == _menus.Count + 2)
                            Environment.Exit(0);
                    } else if (option == _menus.Count + 1) {
                        Environment.Exit(0);
                    }
                    _menus[option - 1].Run();
                    Console.Clear();
                    break;
                }
            }
        }
    }

    
}
